'use strict';

const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const babel = require('babel-register');
const source = require('vinyl-source-stream');
const sass = require('gulp-sass');
const fs = require("fs");
const del = require("del");
const runSequence = require("run-sequence");

gulp.task('js', function () {
	return browserify({entries: './src/app.js', extensions: ['.js'], debug: true})
		.transform('babelify', {presets: ['es2015']})
		.bundle()
		.pipe(source('app.js'))
		.pipe(gulp.dest('./dist/scripts'));
});

gulp.task('js:watch', function () {
	gulp.watch('./src/**/*.js', ['js']);
});

gulp.task('sass', function () {
	return gulp.src('./src/app.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./dist/styles'));
});

gulp.task('build:index', function () {
	return gulp.src('./src/index.html')
		.pipe(gulp.dest('./dist'));
});

gulp.task('build:images', function() {
	return gulp.src('./src/images/*')
		.pipe(gulp.dest('./dist/images'));
});

gulp.task('sass:watch', function () {
	gulp.watch('./src/**/*.scss', ['sass']);
});

gulp.task('html:watch', function () {
	gulp.watch('./src/**/*.html', ['build:index']);
});

gulp.task('clean:dist', function (done) {
	if (fs.existsSync('./dist')) {
		return del('./dist/*');
	}
	done();
});

gulp.task('default', function (done) {
	runSequence(
		'clean:dist',
		[
			'sass',
			'js'
		],
		'build:index',
		'build:images',
		[
			'sass:watch',
			'js:watch',
		  'html:watch'
		],
		function callback() {
			return done;
		}
	)
});
